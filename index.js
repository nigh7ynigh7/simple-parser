
function ignore(char){
    return /[{\}]/.test(char);
}

function split(char){
    return /\s/.test(char);
}

function clean(arr = []){
    if(!arr.length) return arr;
    var pos = [];
    arr.forEach((val, i) =>{
        if(!val) pos.push(i);
    });
    pos.reverse();
    pos.forEach(i => arr.splice(i,1));
    return arr;
}


function grouper(input=''){

    let iMax = input.length;
    let i = 0;

    var level = 0;
    
    function deep(){

        var outer = [];

        var temp = '';

        while(i < iMax){
            let char = input[i];

            if(char == '('){
                level ++;
                outer.push(temp);
                temp = '';
                i++;
                outer.push(deep());
                continue;
            }
            else if(char == ')'){
                i++;
                level --;
                break;
            }
            else if(split(char)){
                outer.push(temp);
                temp = '';
                i++;
                continue;
            }
            temp += char;
            i++;
        }

        if(temp){
            outer.push(temp);
        }

        return clean(outer);
    }

    var output = deep();

    if(level != 0){
        throw new Error('Verify closing and opening brackets - they do not match');
    }

    return output;
}


function isOperator(val=''){
    if(!val || val.length > 1){
        return false;
    }
    return '+-/*'.includes(val);
}

function precedence(val = ''){
    switch(val){
        case '*':
        case '/':
            return 2;
        case '*':
        case '-':
            return 1;
        default:
            return 0;
    }
}

function findHighest(operators=[]){
    if(!operators || !operators.length) return -1;
    let max = 0;
    let highestPos = 0;
    operators.forEach((item, i) => {
        let val = precedence(item);
        if(val > max){
            max = val;
            highestPos = i;
        }
    })
    return highestPos;
}

function applyOperator(iOperator=-1, operators=[], values=[]){
    if(iOperator < 0) return 0;
    if(values.length == 1) return values[0];
    if(operators.length < 1) throw new Error('Invalid readings for operators');



    let a = values[iOperator];
    let b = values[iOperator+1];
    let result = 0;

    if(operators[iOperator] == '+') result = a + b;
    if(operators[iOperator] == '-') result = a - b;
    if(operators[iOperator] == '*') result = a * b;
    if(operators[iOperator] == '/') result = a / b;

    operators.splice(iOperator, 1);
    values.splice(iOperator, 2, result);

}

function evaluate(group = []){
    var terms = [];
    var operators = [];
    
    group.forEach(item => {
        if(isOperator(item)){
            return operators.push(item);
        }

        if(item instanceof Array){
            item = evaluate(item);
        }
        terms.push(Number(item));
    });


    while(terms.length > 1){
        applyOperator(findHighest(operators), operators, terms);
    }
    
    if(!terms.length) return 0;

    return terms[0];
}

let grp = grouper('(9 * 2) / 18');

let evl = evaluate(grp);

console.log(evl);